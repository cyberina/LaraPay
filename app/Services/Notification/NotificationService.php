<?php

namespace App\Services\Notification;

use App\Services\Notification\Contracts\SendMethod;
use App\Services\Notification\Exceptions\NotFoundNotificationProviderException;
use App\Services\Notification\Exceptions\NotSetProviderTypeException;

class NotificationService
{
    private $type;

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function send(array $args)
    {
        if (is_null($this->type)) {
            throw new NotSetProviderTypeException('Provider type is not set');
        }
        $providerInstance = $this->getProvider($this->type);
        return $providerInstance->send($args);

    }

    private function getProvider($type): SendMethod
    {
        $providerClass = NotificationType::getProviderClass($type);
        if (!class_exists($providerClass)) {
            throw new NotFoundNotificationProviderException('Notification provider was not found!');
        }
        return new $providerClass;
    }


}