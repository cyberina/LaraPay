<?php

namespace App\Services\Notification;

use App\Services\Notification\Exceptions\NotFoundNotificationProviderException;
use App\Services\Notification\Providers\Email\EmailProvider;
use App\Services\Notification\Providers\SMS\SMSProvider;
use App\Services\Notification\Providers\Socket\SocketProvider;

class NotificationType
{
    const SMS = 1;
    const EMAIL = 2;
    const SOCKET = 3;

    private static $providers = [
        self::SMS => SMSProvider::class,
        self::EMAIL => EmailProvider::class,
        self::SOCKET => SocketProvider::class
    ];

    public static function getTypes()
    {
        return [
            self::SMS => 'پیامک',
            self::EMAIL => 'ایمیل',
            self::SOCKET => 'سوکت'
        ];
    }

    public static function getProviderClass($type)
    {
        if (array_key_exists($type, self::$providers)) {
            return self::$providers[$type];
        }
        throw new NotFoundNotificationProviderException('Notification provider was not found!');
    }
}