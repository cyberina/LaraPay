<?php


namespace App\Services\Notification\Providers\SMS\Gateways;


interface SMSGateway
{
    public function send(string $to,string $message);
}