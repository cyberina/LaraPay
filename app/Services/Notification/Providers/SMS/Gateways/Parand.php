<?php


namespace App\Services\Notification\Providers\SMS\Gateways;


class Parand implements SMSGateway
{
    private $username;
    private $password;
    private $number;

    public function __construct($config)
    {
        $this->username = $config['username'];
        $this->password = $config['password'];
        $this->number = $config['number'];
    }

    public function send(string $to, string $message)
    {
        $soap = new \SoapClient('https://182.369.158.24/api/send');
        $soap->sendMessage($this->number, $this->username, $this->password, $to, $message);
    }
}