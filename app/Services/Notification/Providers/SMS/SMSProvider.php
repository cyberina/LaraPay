<?php

namespace App\Services\Notification\Providers\SMS;

use App\Services\Notification\Contracts\SendMethod;
use App\Services\Notification\Providers\SMS\Exceptions\NotFoundGatewayDriver;
use App\Services\Notification\Providers\SMS\Exceptions\NotFoundGatewayProvider;
use App\Services\Notification\Providers\SMS\Exceptions\NotSetGatewayProvider;
use App\Services\Notification\Providers\SMS\Gateways\SMSGateway;

class SMSProvider implements SendMethod
{
    public function send(array $args)
    {
        $gatewayInstance = $this->resolveGateway($args);
        $gatewayInstance->send($args['to'], $args['message']);
    }

    private function resolveGateway($args): SMSGateway
    {
        $gateway = $args['gateway'] ?? config('sms.default');
        if (!config()->has("sms.drivers.{$gateway}")) {
            throw new NotFoundGatewayDriver("{$gateway} Driver Not Found");
        }
        if (!config()->has("sms.drivers.{$gateway}.provider")) {
            throw new NotSetGatewayProvider("{$gateway} Provider Not Set");
        }
        $gatewayConfig = config("sms.drivers.{$gateway}");
        if (!class_exists($gatewayConfig['provider'])) {
            throw new NotFoundGatewayProvider("{$gateway} Gateway Provider Not Found");
        }
        return new $gatewayConfig['provider']($gatewayConfig);
    }
}