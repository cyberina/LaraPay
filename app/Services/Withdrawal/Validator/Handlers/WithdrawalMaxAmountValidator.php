<?php


namespace App\Services\Withdrawal\Validator\Handlers;


use App\Services\Withdrawal\Validator\Contracts\Validator;
use App\Services\Withdrawal\Validator\Exceptions\WithdrawalAmountException;
use App\Services\Withdrawal\WithdrawalRequest;

class WithdrawalMaxAmountValidator extends Validator
{

    protected function process(WithdrawalRequest $request)
    {
        $max_amount = $request->getMaxAmount();
        if (intval($request->getAmount()) > $max_amount) {
            throw new WithdrawalAmountException('مبلغ درخواستی بیشتر از حداکثر مبلغ تعیین شده می باشد.');
        }
        return true;
    }
}