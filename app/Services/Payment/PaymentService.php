<?php


namespace App\Services\Payment;


use App\Models\Payment\Payment;
use App\Models\Payment\PaymentType;
use App\Repositories\Invoice\InvoiceRepositoryInterface;
use App\Repositories\Payment\PaymentRepositoryInterface;
use App\Services\Payment\CartToCart\CartToCartPayment;
use App\Services\Payment\Cash\CashPayment;
use App\Services\Payment\Online\OnlinePayment;
use App\Services\Payment\Pos\PosPayment;
use App\Services\Payment\Wallet\WalletPayment;
use Carbon\Carbon;

class PaymentService
{
    private $paymentRepository;

    public function __construct()
    {
        $this->paymentRepository  = resolve(PaymentRepositoryInterface::class);
    }

    public function doPayment(int $invoice_id, int $paymentProvider = PaymentType::GATEWAY)
    {

        $paymentProviderClass   = $this->getPaymentProvider($paymentProvider);
        $paymentProviderHandler = new $paymentProviderClass(0);

        return $paymentProviderHandler->pay();

    }

    public function verifyPayment(array $verifyParams)
    {
        $paymentItem = $this->paymentRepository->findBy([
            'payment_code'   => $verifyParams['paymentCode'],
            'payment_status' => Payment::UNPAID
        ], true, ['invoice']);
        if ( ! $paymentItem) {
            return [
                'success' => false,
                'message' => 'پرداخت شما معتبر نمی باشد'
            ];
        }
        $paymentProvider         = $this->getPaymentProvider($paymentItem->payment_type);
        $paymentProviderInstance = new $paymentProvider($paymentItem->invoice);
        $paymentVerifyResult     = $paymentProviderInstance->verify($paymentItem, $verifyParams);
        $paymentUpdateData       = [];
        if ($paymentVerifyResult['success']) {
            $paymentUpdateData = [
                'payment_ref_num'      => $paymentVerifyResult['ref_num'],
                'payment_trace_number' => $paymentVerifyResult['trance_num'],
                'payment_paid_at'      => Carbon::now()->format('Y-m-d H:i:s'),
                'payment_card_number'  => $paymentVerifyResult['card_number'],
                'payment_status'       => Payment::PAID
            ];
        }
        if (isset($paymentVerifyResult['state'])) {
            $paymentUpdateData['payment_callback_state'] = $paymentVerifyResult['state'];
        }

        $this->paymentRepository->update($paymentItem->payment_id, $paymentUpdateData);
        $paymentVerifyResult['invoice_url'] = $paymentItem->invoice->payment_url;

        return $paymentVerifyResult;

    }

    private function getPaymentProvider(int $paymentProvider)
    {
        return [
            PaymentType::GATEWAY      => OnlinePayment::class,
            PaymentType::WALLET       => WalletPayment::class,
            PaymentType::CASH         => CashPayment::class,
            PaymentType::CART_TO_CART => CartToCartPayment::class,
            PaymentType::POS          => PosPayment::class
        ][$paymentProvider];
    }


}