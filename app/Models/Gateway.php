<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{

    const ACTIVE = 1;
    const INACTIVE = 0;

    protected $primaryKey = 'gateway_id';

    protected $guarded = ['gateway_id'];

    /*Relations*/

    public static function getStatuses()
    {
        return [
            self::ACTIVE   => 'فعال',
            self::INACTIVE => 'غیر فعال '
        ];
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class, 'gateway_plan')->withDefault([
            'gateway_plan_title' => 'طرح نقره ای '
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'gateway_user_id');
    }

    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class, 'withdrawal_gateway_id');
    }

    /*End Relations*/

    public function aggregations()
    {
        return $this->hasMany(GatewayReport::class, 'gateway_report_gateway_id');
    }
}
