<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Contracts\StatisticsRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends AdminController
{


    /**
     * @var StatisticsRepositoryInterface
     */
    private $statisticsRepository;

    public function __construct(StatisticsRepositoryInterface $statisticsRepository)
    {

        $this->statisticsRepository = $statisticsRepository;
    }

    public function index()
    {

        $statistics = new \stdClass();
        $statistics->totalGateways = $this->statisticsRepository->totalGateways();
        $statistics->todayTransactions = $this->statisticsRepository->todayTotalTransactions();
        $statistics->todayWithdrawal = $this->statisticsRepository->todayTotalWithdrawal();
        $statistics->totalPendingGateways = $this->statisticsRepository->totalPendingGateways();

        return view('admin.dashboard.index',compact('statistics'));
    }

    public function statistics()
    {
        return view('admin.dashboard.statistics');
    }
}
