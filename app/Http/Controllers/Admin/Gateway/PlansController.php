<?php

namespace App\Http\Controllers\Admin\Gateway;

use App\Repositories\Contracts\PlanRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlansController extends Controller
{

    /**
     * @var PlanRepositoryInterface
     */
    private $plan_repository;

    public function __construct(PlanRepositoryInterface $plan_repository)
    {

        $this->plan_repository = $plan_repository;
    }

    public function index()
    {
        $plans = $this->plan_repository->all();

        return view('admin.gateway.plan.index', compact('plans'));
    }

    public function create()
    {
        return view('admin.gateway.plan.create');
    }

    public function store(Request $request)
    {
        $newPlan = $this->plan_repository->store([
            'gateway_plan_title'           => $request->plan_title,
            'gateway_plan_commission'      => $request->plan_commission,
            'gateway_plan_withdrawal_rate' => $request->plan_withdrawal_rate,
            'gateway_plan_withdrawal_max'  => $request->plan_withdrawal_max
        ]);
        if ($newPlan) {
            return back()->with('success', 'طرح جدید با موفقیت ایجاد شد');
        }
    }
}
