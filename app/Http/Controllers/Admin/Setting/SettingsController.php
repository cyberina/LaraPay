<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Repositories\Contracts\SettingsRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * @var SettingsRepositoryInterface
     */
    private $settings_repository;

    public function __construct(SettingsRepositoryInterface $settings_repository)
    {

        $this->settings_repository = $settings_repository;
    }

    public function index()
    {
        $settings = $this->settings_repository->all();

        return view('admin.settings.index', compact('settings'));

    }

    public function create()
    {
        return view('admin.settings.create');
    }

    public function store(Request $request)
    {
        //TODO validate request
        $newSetting = $this->settings_repository->store($request->only(['setting_key', 'setting_value']));
        if($newSetting)
        {
            return back()->with('success','تنظیمات جدید با موفقیت ایجاد شد!');
        }
    }
}
