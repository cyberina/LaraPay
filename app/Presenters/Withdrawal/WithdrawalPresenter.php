<?php


namespace App\Presenters\Withdrawal;


use App\Helpers\Currency\PersianCurrency;
use App\Helpers\Format\Number;
use App\Models\Withdrawal;
use App\Presenters\Contracts\Presenter;

class WithdrawalPresenter extends Presenter
{
    public function amount()
    {
        return PersianCurrency::toman($this->entity->withdrawal_amount);
    }

    public function commission()
    {
        return Number::persianNumbers($this->entity->withdrawal_commission) . ' % ';
    }

    public function create()
    {
        return Number::persianNumbers(verta($this->entity->created_at));
    }

    public function update()
    {
        return Number::persianNumbers(verta($this->entity->updated_at));
    }

    public function adminStatus()
    {
        $status = $this->entity->withdrawal_status;
        $result = "";
        switch ($status) {
            case Withdrawal::PENDING:
                $result = '<span class="badge badge-warning">در حال بررسی</span>';
                break;
            case Withdrawal::DONE:
                $result = '<span class="badge badge-success">انجام شد</span>';
                break;
            case Withdrawal::REJECTED:
                $result = '<span class="badge badge-danger">رد شد</span>';
                break;
            default:
                $result = '<span class="badge badge-dark">نامشخص</span>';
            break;
        }
        return $result;
    }

    public function adminOperations()
    {
        $status = $this->entity->withdrawal_status;
        if($status == Withdrawal::PENDING)
        {
            $withdrawal = $this->entity;
            return view('admin.withdrawal.operations',compact('withdrawal'))->render();
        }
        return "";
    }
}