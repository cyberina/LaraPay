<?php
return [
    'default' => env('SMS_DRIVER', 'parand'),

    'drivers' => [
        'parand' => [
            'number' => '50003698',
            'username' => 'user564a98c4as98d',
            'password' => '3as4d897d9',
            'provider' => \App\Services\Notification\Providers\SMS\Gateways\Parand::class
        ],
        'farapayamak' => [
            'number' => '3004565',
            'apiKey' => 'a5sas4da+s45cas+f4as8f',
            'provider' => \App\Services\Notification\Providers\SMS\Gateways\FaraPayamak::class
        ]
    ]
];