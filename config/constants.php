<?php
return [
    'withdrawal' => [
        'amount_min'  => 100000,
        'count_limit' => 3
    ]
];